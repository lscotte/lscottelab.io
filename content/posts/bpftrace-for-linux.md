+++
title = "bpftrace for Linux"
description = "bpftrace for Linux - It's like D-Trace 2.0"
date = 2018-10-09T14:29:15-07:00
weight = 20
draft = false
toc = false
tags = ["linux","bpf"]
categories = ["linux", "posts"]
+++

This is a really big deal and the culmination of many years of work, but we now
have a de-facto standard for Linux tracing, via
[bpftrace](https://github.com/iovisor/bpftrace) - a front-end (really, it's a
high-level language) for eBPF/BCC.

I cannot do a better job than
[Brendan at explaining why this is so important](http://www.brendangregg.com/blog/2018-10-08/dtrace-for-linux-2018.html).
