+++
title = "borgsnap"
description = "A script for doing borg backup of ZFS filesystems"
date = 2018-10-07T14:30:36-07:00
weight = 20
draft = false
toc = false
tags = ["zfs", "borg", "rsync.net", "linux"]
categories = ["linux", "projects"]
+++

I wrote [borgsnap](https://github.com/scotte/borgsnap) to make it easy to
do backups of a ZFS filesystem with borg to a local drive and/or rsync.net.
Backups are done via automated ZFS snapshots. This is some pretty powerful
stuff that's relatively simple to setup.

rsync.net has some [very favorable pricing](https://rsync.net/products/attic.html)
for storing borg backups - 2 cents/GB as of this writing (they just lowered
the pricing from 3 cents/GB a few weeks ago). And no bandwidth or other charges,
so it's pretty appealing. With borg, it's easy to mount up the remote rsync.net
backups to test recovery.
