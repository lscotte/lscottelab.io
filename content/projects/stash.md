+++
title = "stash"
description = "A wrapper around stow and git for managing dotfiles"
date = 2018-10-07T14:46:11-07:00
weight = 20
draft = false
toc = false
tags = ["git", "stow", "linux"]
categories = ["linux", "projects"]
+++

I wrote [stash](https://github.com/scotte/stash) to make it a bit easier to
manage my dotfiles across multiple systems. stow is great for this, but I
wanted to make it a bit simpler and with configurations in a branch-per-system
git repository.
