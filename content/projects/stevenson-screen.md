+++
title = "Outdoor Thermometer Stevenson Screen"
description = "A 3D printer design for a thermometer housing"
date = 2018-10-07T15:03:34-07:00
weight = 20
draft = false
toc = false
tags = ["3d"]
categories = ["3d", "projects"]
+++

![](https://cdn.thingiverse.com/renders/64/db/61/bd/44/e2a11ae31911305fef1956d0ef5cc7a7_preview_featured.jpg)

I created [this Stevenson Screen design](https://www.thingiverse.com/thing:3034323)
for our outdoor thermometer so it would be a bit less sensitive to false
readings.
