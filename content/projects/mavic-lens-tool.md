+++
title = "Mavic Air Lens Removal Tool"
description = "A 3D printer design for a tool to remove the Mavic Air Lens"
date = 2018-10-07T14:56:09-07:00
weight = 20
draft = false
toc = false
tags = ["drones", "mavic", "3d"]
categories = ["drones", "3d", "projects"]
+++

![](https://cdn.thingiverse.com/renders/54/69/c9/cc/74/d248da51bda9a893a44fcf96d01f5e92_preview_featured.jpg)

If you have a Mavic Air sUAS and a 3D printer, [this tool design](https://www.thingiverse.com/thing:3007540)
will make it much easier to remove the factory camera lens so you can install
an ND or other lens filter. The original lens is screwed on very tightly and is
difficult to remove without some mechanical advantage.

